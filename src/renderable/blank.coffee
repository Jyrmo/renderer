import Renderable from '../renderable'

class Blank extends Renderable
  # TODO: assume @color is [r, g, b, a] with 0 < r, g, b, a < 1 for now
  # TODO: set color in shader
  constructor: (entity, @color, @shader) -> super entity

  render: ->
    # TODO: reusing the buffer? Or maybe it belongs in the shader?
    posBuffer = @createVertexPositionsBuffer()
    @shader.setVertexPositions posBuffer
    @gl.drawArrays @gl.TRIANGLE_STRIP, 0, 4

  createVertexPositionsBuffer: ->
    vertexPositions = @calculateVertexPositions()
    # TODO: can possibly factor this out into a buffer utility function?
    posBuffer = @gl.createBuffer()
    @gl.bindBuffer @gl.ARRAY_BUFFER, posBuffer
    @gl.bufferData @gl.ARRAY_BUFFER, vertexPositions, @gl.STATIC_DRAW
    posBuffer

  calculateVertexPositions: ->
    # TODO: apply transform rotation and scaling
    # TODO: account for transform z coordinate as well
    { x, y } = @entity.transform.pos
    new Float32Array [
      x + 20, y + 20,
      x - 20, y + 20,
      x + 20, y - 20,
      x - 20, y - 20,
    ]

export default Blank