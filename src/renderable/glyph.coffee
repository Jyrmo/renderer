import { cloneDeep } from 'lodash'

import calculateTransformMatrix from '../calculate-transform-matrix'
import { default as SpriteRenderable } from './sprite'

# TODO: rm possibly?

class Glyph extends SpriteRenderable
  constructor: (entity, glyph, tint) ->
    super entity, glyph.sprite, tint
    @glyph = glyph

  setTransform: ->
    glyphTransform = @adjustTransform()
    transform = calculateTransformMatrix glyphTransform
    transformLoc = @gl.getUniformLocation @shader, 'transform'
    @gl.uniformMatrix3fv transformLoc, false, transform

  adjustTransform: ->
    glyphTransform = cloneDeep @entity.transform
    glyphTransform.pos[0] += @glyph.xOffset / 2
    glyphTransform.pos[1] -= @glyph.yOffset / 2
    glyphTransform.scale[0] = @sprite.width
    glyphTransform.scale[1] = @sprite.height
    glyphTransform

export default Glyph