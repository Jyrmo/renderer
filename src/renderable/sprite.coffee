import calculateTransformMatrix from '../calculate-transform-matrix'
# TODO: line too long
import calculateTextureTransformMatrix from '../calculate-texture-transform-matrix'
import Renderable from '../renderable'
import unitSquare from '../vao/unit-square'
import {
  bufferVertices, attribPointer, buildTexture, getGl, useShader
} from '../gl-wrapper'
import unitSquareTexCoords from '../unit-square-tex-coords'

shaderName = 'sprite'

class Sprite extends Renderable
  # sprite format: { img, x, y, width, height }
  constructor: (entity, @sprite, @tint = [1, 1, 1, 1]) ->
    super entity, shaderName

  render: ->
    useShader @shader
    @setTransform()
    @setTint()
    @setVao()
    @setTexture()
    @draw()

  setTransform: ->
    transform = calculateTransformMatrix @entity.transform
    transformLoc = @gl.getUniformLocation @shader, 'transform'
    @gl.uniformMatrix3fv transformLoc, false, transform

  setTint: ->
    tintLoc = @gl.getUniformLocation @shader, 'tint'
    @gl.uniform4fv tintLoc, @tint

  setVao: ->
    bufferVertices unitSquare
    attribPointer @shader, 'pos', 2

  setTexture: ->
    # TODO: fine tune texture transform matrix
    bufferVertices unitSquareTexCoords
    attribPointer @shader, 'texPos', 2
    texTransformLoc = @gl.getUniformLocation @shader, 'texTransform'
    texTransform = calculateTextureTransformMatrix @sprite
    @gl.uniformMatrix3fv texTransformLoc, false, texTransform
    buildTexture @sprite.img

  draw: -> @gl.drawArrays @gl.TRIANGLES, 0, 6

export default Sprite