import { mat3 } from 'gl-matrix'

# sprite format: { img, x, y, width, height }
calculateTextureTransformMatrix = (sprite) ->
  # DBZ
  { img, x, y, width, height } = sprite
  { naturalWidth, naturalHeight } = img
  scaling = [width / naturalWidth, height / naturalHeight]
  translation = [x / width, y / height]
  matTransform = []
  mat3.fromScaling matTransform, scaling
  mat3.translate matTransform, matTransform, translation
  matTransform

export default calculateTextureTransformMatrix