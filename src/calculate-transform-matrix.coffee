import { mat3 } from 'gl-matrix'

import { getResolution } from './gl-wrapper'

# TODO: vec2, vec3, vec4 data types with es6 style x, y, z getters and setters

# TODO: rn -> calculateTransformMatrix2D
calculateTransformMatrix = (transform) ->
  { pos, rot, scale } = transform
  matTransform = []
  [resX, resY] = getResolution()
  if resX is 0 or resY is 0
    # TODO: special error class
    throw new Error "Invalid resolution (#{resX}, #{resY})."
  resolutionScaling = [2 / resX, 2 / resX]
  mat3.fromScaling matTransform, resolutionScaling
  mat3.translate matTransform, matTransform, pos
  mat3.rotate matTransform, matTransform, rot
  mat3.scale matTransform, matTransform, scale
  matTransform

export default calculateTransformMatrix