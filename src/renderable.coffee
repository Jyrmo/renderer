import { getGl } from './gl-wrapper'
import { get as getShader } from './shaders'

class Renderable
  constructor: (@entity, shaderName) ->
    @gl = getGl()
    @entity.renderable = this
    @shader = getShader shaderName

  render: ->

export default Renderable