import { uniqueId } from 'lodash'

namePrefix = 'sh_'

shaders = {}

export set = (shader, name = uniqueId namePrefix) ->
  shaders[name] = shader
  name

export setAll = (shadersParam) -> shaders = shadersParam

export get = (name) -> shaders[name]

export getAll = -> shaders

export clear = -> shaders = {}