import { mat3 } from 'gl-matrix'

import { get as getShader } from '../shaders'
import {
  useShader, bufferVertices, getResolution, getGl, attribPointer
} from '../gl-wrapper'

shaderName = 'blank'

buildVao = (pt1, pt2) ->
  new Float32Array [
    pt1[0], pt1[1],
    pt2[0], pt2[1]
  ]

# TODO: lines primitive

export default drawLine = (pt1, pt2, color) ->
  # TODO: factor out functions
  vao = buildVao pt1, pt2
  console.log vao
  bufferVertices vao
  # TODO: dedup with calculateTransformMatrix
  transform = []
  [resX, resY] = getResolution()
  resolutionScaling = [2 / resX, 2 / resX]
  mat3.fromScaling transform, resolutionScaling
  # TODO: where should we set shader?
  shader = getShader shaderName
  useShader shader
  gl = getGl()
  transformLoc = gl.getUniformLocation shader, 'transform'
  gl.uniformMatrix3fv transformLoc, false, transform
  colorLoc = gl.getUniformLocation shader, 'color'
  gl.uniform4fv colorLoc, color
  attribPointer shader, 'pos', 2
  gl.drawArrays(gl.LINES, 0, 2)