import { init as initGlWrapper } from './gl-wrapper'
import * as shaders from './shaders'
import * as drawPrimitive from './draw-primitive'

export { shaders, drawPrimitive }
# TODO: renderables container?
export { default as SpriteRenderable } from './renderable/sprite'
export { default as GlyphRenderable } from './renderable/glyph'
export { getGl, clear } from './gl-wrapper'

# TODO: drawPrimitive functions
# TODO: coffeelint
# TODO: remove unnecessary dependencies
# TODO: precompiled shaders
# TODO: if we compile then perhaps all dependencies can be dev dependencies???
# TODO: remove old stuff from lib folder
# TODO: get blank renderable working again

# TODO: allow shaders object in spec
export init = (spec) ->
  { canvas, resolution } = spec
  # TODO: play around with context attributes (2nd param to getContext)
  gl = canvas.getContext 'webgl2'
  conf = { gl, resolution }
  initGlWrapper conf
