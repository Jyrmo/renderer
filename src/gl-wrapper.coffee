import ShaderCompiler from './shader-compiler'
import shaderSpecs from './shader-specs'

gl = null

resolution = [1, 1]

export init = (conf) ->
  { gl, shaderSpec, resolution } = conf
  [resX, resY] = resolution
  gl.viewport 0, 0, resX, resY
  gl.clearColor 0, 0, 0, 1

export getGl = -> gl

export getResolution = -> resolution

export bufferVertices = (vertices) ->
  buffer = gl.createBuffer()
  gl.bindBuffer gl.ARRAY_BUFFER, buffer
  gl.bufferData gl.ARRAY_BUFFER, vertices, gl.STATIC_DRAW

# TODO: get shader from gl???
export attribPointer = (shader, attrName, stride) ->
  loc = gl.getAttribLocation shader, attrName
  gl.enableVertexAttribArray loc
  gl.vertexAttribPointer loc, stride, gl.FLOAT, false, 0, 0

# TODO: pass in texture number?
# Sprite { img, x, y, width, height }
export buildTexture = (img) ->
  texture = gl.createTexture()
  gl.activeTexture gl.TEXTURE0 + 0
  gl.bindTexture gl.TEXTURE_2D, texture
  gl.texParameteri gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE
  gl.texParameteri gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE
  gl.texParameteri gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST
  gl.texParameteri gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST
  width = img.naturalWidth
  height = img.naturalHeight
  gl.texImage2D gl.TEXTURE_2D, 0, gl.RGBA, width, height, 0, gl.RGBA,
    gl.UNSIGNED_BYTE, img

export clear = -> gl.clear gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT

# TODO: reference shader by name only???
export useShader = (shader) -> gl.useProgram shader