# TODO: mv to asset loader

class ShaderCompiler
  constructor: (@gl) ->

  compileShaderProgram: (vsSrc, fsSrc) ->
    vs = @compileVertexShader vsSrc
    glError = @gl.getError()
    if glError
      throw new Error "GL error compiling vertex shader: #{glError}"
    fs = @compileFragmentShader fsSrc
    glError = @gl.getError()
    if glError
      throw new Error "GL error compiling fragment shader: #{glError}"
    shader = @gl.createProgram()
    @gl.attachShader shader, vs
    @gl.attachShader shader, fs
    @gl.linkProgram shader
    if not @gl.getProgramParameter shader, @gl.LINK_STATUS
      shaderLog = @gl.getProgramInfoLog shader
      throw new Error "Cannot init shader program: #{shaderLog}"
    shader

  compileVertexShader: (src) -> @compileShader src, @gl.VERTEX_SHADER

  compileFragmentShader: (src) -> @compileShader src, @gl.FRAGMENT_SHADER

  compileShader: (src, type) ->
    compiledShader = @gl.createShader type
    @gl.shaderSource compiledShader, src
    @gl.compileShader compiledShader
    compiledShader

export default ShaderCompiler