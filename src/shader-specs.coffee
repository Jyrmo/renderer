# TODO: pull shader source from separate files
# TODO: rm

spriteVsSrc = """#version 300 es

in vec2 pos;
in vec2 texPos;

uniform mat3 transform;
uniform vec4 tint;

uniform mat3 texTransform;

out vec2 fragTexPos;
out vec4 fragTint;

void main() {
  vec2 transformedPos = (transform * vec3(pos, 1)).xy;
  gl_Position = vec4(transformedPos, 0, 1);

  fragTexPos = (texTransform * vec3(texPos, 1)).xy;
  fragTint = tint;
}
"""

spriteFsSrc = """#version 300 es

precision mediump float;

in vec2 fragTexPos;
in vec4 fragTint;

uniform sampler2D image;

out vec4 outColor;

void main() {
  vec4 texelColor = texture(image, fragTexPos);
  outColor = texelColor * fragTint;
}"""

blankVsSrc = """#version 300 es

in vec2 pos;

uniform mat3 transform;

void main() {
  vec2 transformedPos = (transform * vec3(pos, 1)).xy;
  gl_Position = vec4(transformedPos, 0, 1);
}
"""

blankFsSrc = """#version 300 es

precision mediump float;

uniform vec4 color;

out vec4 outColor;

void main() {
  outColor = color;
}
"""

export default {
  sprite: {
    vsSrc: spriteVsSrc
    fsSrc: spriteFsSrc
  }
  blank: {
    vsSrc: blankVsSrc
    fsSrc: blankFsSrc
  }
}