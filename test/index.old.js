const bmpFont = require('@jyrmo/bmp-font')

const renderer = require('../lib/index')

const Font = bmpFont.default
const { SpriteRenderable, GlyphRenderable } = renderer

let renderable

let font = null

const req = new XMLHttpRequest()

const spriteAnimationFps = 12
const mpf = 1000 / spriteAnimationFps

const frameIdx = {
  row: 0,
  col: 0
}

const advanceSpriteAnimation = () => {
  frameIdx.col++
  if (frameIdx.col > 2) {
    frameIdx.col = 0
    frameIdx.row++
    if (frameIdx.row > 3) {
      frameIdx.row = 0
    }
  }

  renderable.sprite.x = frameIdx.col * 32
  renderable.sprite.y = frameIdx.row * 32
}

let frameStartTime = null

const advanceAnimationTimer = (timestamp) => {
  if (!frameStartTime) {
    frameStartTime = timestamp
  }

  const timeElapsed = (timestamp) - frameStartTime
  
  if (timeElapsed >= mpf) {
    advanceSpriteAnimation()
    frameStartTime = timestamp
  }
}

const image = new Image()
image.src = 'zap-black-outline.png'

const imgContra = new Image()
imgContra.src = 'contra.jpg'

const imgFont = new Image
imgFont.src = 'Consolas-72.png'

let fileContent = null

const buildEntity = () => {
  const transform = {
    pos: [0, 0],
    scale: [1, 1],
    rot: 0
  }
  const entity = { transform }

  return entity
}

const buildEntitySprite = (img) => {
  return {
    img,
    x: 0,
    y: 0,
    width: 32,
    height: 32,
  }
}

const buildContra = () => {
  const transform = {
    pos: [-100, 120],
    scale: [200, 160],
    rot: 0.3
  }
  const entity = { transform }

  return entity
}

const buildContraSprite = (img) => {
  return {
    img,
    x: 12,
    y: 12,
    width: 200,
    height: 100,
  }
}

const setEntityTransform = (entity, translation, rotation, scale) => {
  const transform = entity.transform
  transform.pos = translation
  transform.scale = scale
  transform.rot = rotation
}

const buildGlyph = () => {
  const glyphInfo = font.getGlyph('J')
  const transform = {
    pos: [200, -200],
    scale: [50, 50],
    rot: 0
  }
  const glyph = { transform }
  const glyphRenderable = new GlyphRenderable(glyph, glyphInfo)

  return glyph
}

let entity
let contra
let contraRenderable

let isPlaying = true
let frameCount = 0

const tick = (timestamp) => {
  frameCount++

  const translation = [300 * Math.sin(timestamp / 700), 200 * Math.cos(timestamp / 750)]
  const rotation = 4 * Math.sin(timestamp / 900)
  const scale = [200 + 60 * Math.sin(timestamp / 1000), 120 + 50 * Math.cos(timestamp / 650)]
  setEntityTransform(entity, translation, rotation, scale)

  // const alpha = Math.sin(timestamp / 725)
  const alpha = 0.8
  const blue = Math.sin(timestamp / 850)
  renderable.tint = [1, 1, blue, alpha]

  advanceAnimationTimer(timestamp)

  const glyph = buildGlyph()
  
  renderer.clear()
  
  glyph.renderable.render()
  renderable.render()
  contraRenderable.render()

  if (frameCount >= 1000) {
    isPlaying = false
  }

  if (isPlaying) {
    requestAnimationFrame(tick)
  }
}

const start = () => {
  const canvas = document.getElementById('canvas')
  const shaderSpec = { builtIn: 'sprite' }
  const resolution = [800, 600]
  renderer.init({canvas, shader: shaderSpec, resolution})

  font = new Font(imgFont, fileContent)

  entity = buildEntity()
  const entitySprite = buildEntitySprite(image)
  renderable = new SpriteRenderable(entity, entitySprite)

  contra = buildContra()
  const contraSprite = buildContraSprite(imgContra)
  contraRenderable = new SpriteRenderable(contra, contraSprite)

  requestAnimationFrame(tick)
}

let numLoaded = 0

const addLoaded = () => {
  numLoaded++
  if (numLoaded === 4) {
    start()
  }
}

imgContra.onload = () => {
  addLoaded()
}

image.onload = () => {
  addLoaded()
}

imgFont.onload = () => {
  addLoaded()
}

req.open('GET', 'Consolas-72.fnt', true)
req.setRequestHeader('Content-Type', 'text/xml')

req.onload = () => {
  const parser = new DOMParser()
  fileContent = req.responseText

  addLoaded()
}

req.send()

window.play = () => {
  frameCount = 0
  isPlaying = true
  requestAnimationFrame(tick)
}

window.stop = () => {
  isPlaying = false
}

window.togglePlaying = () => {
  if (isPlaying) {
    window.stop()
  } else {
    window.play()
  }
}