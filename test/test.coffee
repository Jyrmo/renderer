{ mat3, vec3 } = require 'gl-matrix'

scaling = [1.0 / 96.0, 1.0 / 128.0]
pos = [32, 32]

mat = []
mat3.fromScaling mat, scaling
mat3.translate mat, mat, pos

point = [0, 0, 1]
coords = []
vec3.transformMat3 coords, point, mat

console.log mat
console.log coords