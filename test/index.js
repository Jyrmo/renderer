const renderer = require('../lib/index')
const ShaderCompiler = require('../lib/shader-compiler').default

blankVsSrc = `#version 300 es

in vec2 pos;

uniform mat3 transform;

void main() {
  vec2 transformedPos = (transform * vec3(pos, 1)).xy;
  gl_Position = vec4(transformedPos, 0, 1);
}
`

blankFsSrc = `#version 300 es

precision mediump float;

uniform vec4 color;

out vec4 outColor;

void main() {
  outColor = color;
}
`

spriteVsSrc = `#version 300 es

in vec2 pos;
in vec2 texPos;

uniform mat3 transform;
uniform vec4 tint;

uniform mat3 texTransform;

out vec2 fragTexPos;
out vec4 fragTint;

void main() {
  vec2 transformedPos = (transform * vec3(pos, 1)).xy;
  gl_Position = vec4(transformedPos, 0, 1);

  fragTexPos = (texTransform * vec3(texPos, 1)).xy;
  fragTint = tint;
}
`

spriteFsSrc = `#version 300 es

precision mediump float;

in vec2 fragTexPos;
in vec4 fragTint;

uniform sampler2D image;

out vec4 outColor;

void main() {
  vec4 texelColor = texture(image, fragTexPos);
  outColor = texelColor * fragTint;
}`

const image = new Image()
image.src = 'zap-black-outline.png'

const start = () => {
  const canvas = document.getElementById('canvas')
  const resolution = [800, 600]
  renderer.init({canvas, resolution})
  const gl = renderer.getGl()

  const shaderCompiler = new ShaderCompiler(gl)
  const blankShader = shaderCompiler.compileShaderProgram(blankVsSrc, blankFsSrc)
  renderer.shaders.set(blankShader, 'blank')
  const spriteShader = shaderCompiler.compileShaderProgram(spriteVsSrc, spriteFsSrc)
  renderer.shaders.set(spriteShader, 'sprite')

  const transform = {
    pos: [-100, 200],
    scale: [64, 64],
    rot: 0.2
  }
  const entity = { transform }
  const sprite = {
    img: image,
    x: 32,
    y: 32,
    width: 32,
    height: 32,
  }
  new renderer.SpriteRenderable(entity, sprite)

  renderer.clear()
  entity.renderable.render()

  renderer.drawPrimitive.line([-100, -150], [200, 220], [0.1, 0.7, 0.1, 0.9])

  // const vertices = [
  //   -0.7,-0.1,
  //   -0.3,0.6,
  //   -0.3,-0.3,
  //   0.2,0.6,
  //   0.3,-0.3,
  //   0.7,0.6 
  // ]
  // const transform = [
  //   1, 0, 0,
  //   0, 1, 0,
  //   0, 0, 1
  // ]

  // const vertex_buffer = gl.createBuffer()
  // gl.bindBuffer(gl.ARRAY_BUFFER, vertex_buffer)
  // gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertices), gl.STATIC_DRAW)

  // const shader = renderer.getShader()
  // const transformLoc = gl.getUniformLocation(shader, 'transform')
  // gl.uniformMatrix3fv(transformLoc, false, transform)
  // const colorLoc = gl.getUniformLocation(shader, 'color')
  // gl.uniform4fv(colorLoc, [0.7, 0.2, 0.2, 0.9])
  // const posLoc = gl.getAttribLocation(shader, 'pos')
  // gl.vertexAttribPointer(posLoc, 2, gl.FLOAT, false, 0, 0)
  // gl.enableVertexAttribArray(posLoc)

  // gl.clearColor(0.1, 0.1, 0.1, 0.9)
  // gl.enable(gl.DEPTH_TEST)
  // gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)
  // gl.viewport(0, 0, 800, 600)

  // gl.drawArrays(gl.LINES, 0, 6)
}

image.onload = () => {
  start()
}
window.onload = () => {
}